//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labPoesyForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
inline int Low(const UnicodeString &)
{
	#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
	#else
	return 0;
	#endif
}
inline int High(const UnicodeString &S)
{
	#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
	#else
	return S.Length()-1;
	#endif
}
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->First();
	UnicodeString x;
	//str
	for(int i = 0; i<meFull->Lines->Count; i++)
	{
		x = meFull->Lines->Strings[i];
		if (i%2 ==1 ) {
		   for (int j = Low(x); j<=High(x); j++) {
			   if(x[j] != ' ')
			   x[j]='x';
		   }
		}
		me1->Lines->Add(x);
	}
	bool xFlag;
   for(int i = 0; i<meFull->Lines->Count; i++)
	{
		x = meFull->Lines->Strings[i];
		xFlag = false;

		   for (int j = Low(x); j<=High(x); j++) {
			   if((xFlag)&&(x[j]!=' '))
					x[j]='x';
			   if((!xFlag) && (x[j] == ' '))
					xFlag = true;
		   }

		 me2->Lines->Add(x);
	}
		int moreFlag;
	   for(int i = 0; i<meFull->Lines->Count; i++)
	{
		x = meFull->Lines->Strings[i];
		xFlag = false;
		moreFlag=0;
		   for (int j = Low(x); j<=High(x); j++) {
			   if((xFlag)&&(x[j]!=' ')){
					x[j]='x';
			   }

			   if((!xFlag) && (moreFlag==0)){
					xFlag = true;
					moreFlag=1;
			   }

			   if((xFlag)&&(x[j]==' '))
			   {
					xFlag = false;
			   		moreFlag=0;
			   }
		   }

		 me3->Lines->Add(x);
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"Privet");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSizeInClick(TObject *Sender)
{
	meFull->Font->Size +=5;
	me1->Font->Size +=5;
	me2->Font->Size +=5;
    me3->Font->Size +=5;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSizeoutClick(TObject *Sender)
{
	meFull->Font->Size -=5;
	me1->Font->Size -=5;
	me2->Font->Size -=5;
	me3->Font->Size -=5;
}
//---------------------------------------------------------------------------
