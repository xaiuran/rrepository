//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "formMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button1MouseEnter(TObject *Sender)
{
       TButton *x= ((TButton *) Sender);
	  x->Margins->Rect = TRect(0,0,0,0);
	  x->TextSettings->Font->Size +=10;
	  x->TextSettings->Font->Style = x->TextSettings->Font->Style << TFontStyle::fsBold;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1MouseLeave(TObject *Sender)
{
		TButton *x= ((TButton *) Sender);
	  x->Margins->Rect = TRect(5,5,5,5);
	  x->TextSettings->Font->Size -=10;
	  x->TextSettings->Font->Style = x->TextSettings->Font->Style >> TFontStyle::fsBold;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Image1MouseEnter(TObject *Sender)
{
	((TImage*)Sender)->Margins->Rect=TRect(10,10,10,10);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image1MouseLeave(TObject *Sender)
{
	((TImage*)Sender)->Margins->Rect=TRect(0,0,0,0);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ToolBar1MouseEnter(TObject *Sender)
{
	FloatAnimation1->Start();
//	if(FloatAnimation1->StopValue==ToolBar1->Position->X)
//	{
//        FloatAnimation1->Stop();
//    }

}
//---------------------------------------------------------------------------

void __fastcall Tfm::ToolBar1MouseLeave(TObject *Sender)
{
	//FloatAnimation1->AutoReverse=true;
	FloatAnimation2->Start();
}
//---------------------------------------------------------------------------

