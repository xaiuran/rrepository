//---------------------------------------------------------------------------

#ifndef formMainH
#define formMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TImage *Image1;
	TLayout *Layout1;
	TLayout *Layout2;
	TImage *Image2;
	TToolBar *ToolBar1;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TStyleBook *StyleBook1;
	void __fastcall Button1MouseEnter(TObject *Sender);
	void __fastcall Button1MouseLeave(TObject *Sender);
	void __fastcall Image1MouseEnter(TObject *Sender);
	void __fastcall Image1MouseLeave(TObject *Sender);
	void __fastcall ToolBar1MouseEnter(TObject *Sender);
	void __fastcall ToolBar1MouseLeave(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
