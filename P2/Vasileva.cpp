//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Vasileva.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem2Click(TObject *Sender)
{
 if (OpenDialog1->Execute()) Memo1->Lines->LoadFromFile(OpenDialog1->FileName);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem3Click(TObject *Sender)
{
  if (SaveDialog1->Execute()) Memo1->Lines->SaveToFile(SaveDialog1->FileName);
}
//---------------------------------------------------------------------------


void __fastcall TForm2::MenuItem5Click(TObject *Sender)
{
 Application->Terminate();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem10Click(TObject *Sender)
{
Memo1->CutToClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem7Click(TObject *Sender)
{
Memo1->CopyToClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem8Click(TObject *Sender)
{
Memo1->PasteFromClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem9Click(TObject *Sender)
{
Memo1->CutToClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem11Click(TObject *Sender)
{
PrinterSetupDialog1->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::MenuItem4Click(TObject *Sender)
{
PrintDialog1->Execute();
}
//---------------------------------------------------------------------------


