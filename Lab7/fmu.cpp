//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->First();
	tc->TabPosition= TTabPosition::None;
	pb->Max = tc->TabCount-1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	me->Lines->Clear();
	tc->Next();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button5Click(TObject *Sender)
{
	//
	tc->Next();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButtonAllClick(TObject *Sender)
{
	UnicodeString x;
	x= ((TControl *)Sender)->Tag == 1 ? L"�����": L"�������";
	if (x==L"�����") {
	  FCountTrue++;
	}
	else
	{
      FCountFalse++;
    }
	me->Lines->Add(Format(L"������ %s - %s",ARRAYOFCONST((tc->ActiveTab->Text,x)) ));
	tc->Next();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	FCountTrue=0;
	FCountFalse=0;
	tc->First();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	pb->Value = tc->ActiveTab->Index;
	laCountQ->Text=Format(L"(%d �� %d)",ARRAYOFCONST((tc->ActiveTab->Index, tc->TabCount-2)) );
	laCountQ->Visible = (tc->ActiveTab != tiMenu) && (tc->ActiveTab !=tiResult);
	laTrue->Text=Format(L"�����=%d",ARRAYOFCONST((FCountTrue)));
	laFalse->Text=Format(L"�������=%d",ARRAYOFCONST((FCountFalse)));
	if (tc->ActiveTab == tiResult) {
		Image2->Visible = (FCountTrue >FCountFalse);
		Image3->Visible = !Image2->Visible;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"���� ��� ������ � ��������");
}
//---------------------------------------------------------------------------
