//---------------------------------------------------------------------------
#include "Unit2.cpp"
#include <fmx.h>
#pragma hdrstop
#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------



void __fastcall Tfm::Button1Click(TObject *Sender)
{
    TabControl1->GotoVisibleTab(TabItem2->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	TabControl1->TabPosition = TTabPosition :: None;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
TabControl1->GotoVisibleTab(TabItem3->Index);
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button3Click(TObject *Sender)
{
TabControl1->GotoVisibleTab(TabItem1->Index);
}
//---------------------------------------------------------------------------

