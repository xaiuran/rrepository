//---------------------------------------------------------------------------

#ifndef fmMainH
#define fmMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buRestart;
	TButton *buAbout;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buYes;
	TButton *buNo;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TLabel *laCorrect;
	TLabel *laWrong;
	TLabel *Label3;
	TLabel *laCode;
	TLabel *laTime;
	TTimer *tm;
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
private:
	int FCountCorrect;
	int FCountWrong;
    int FCountWin;
	bool FAnswerCorrect;
	void DoReset();
	void DoContinue();
	void DoAnswer(bool aValue);
	TDateTime FTimerStart;

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
