//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect=0;
	FCountWrong=0;
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoContinue()
{
	int FCountNew1;
	int FCountNew2;
	FTimerStart = Now();
	tm->Enabled = true;

//	if(FCountWin>0)
//	{
	laCorrect->Text = Format(L"����� = %d",ARRAYOFCONST((FCountCorrect)));
	laWrong->Text = Format(L"������� = %d",ARRAYOFCONST((FCountWrong)));
	if(FCountWin>0)
	{
		 FCountNew1=Random(20*(FCountWin+1));
	}
	else
	{
		 FCountNew1=Random(20/(FCountWin*(-1)+1));
	}
	if(FCountWin>0)
	{
		 FCountNew2=Random(20*(FCountWin+1));
	}
	else
	{
		 FCountNew2=Random(20/(FCountWin*(-1)+1));
	}
	int xValue1 = FCountNew1;

	int xValue2 = FCountNew2;
	int xSign = (Random(2) ==1)?1:-1;
	int xResult = xValue1+xValue2;
	int xResultNew = (Random(2) == 1)?xResult : xResult + (Random(10) * xSign);

	FAnswerCorrect = (xResult == xResultNew);
	laCode->Text = Format("%d + %d = %d",ARRAYOFCONST((xValue1, xValue2, xResultNew)));
//	}
//	else
//	{
//	laCorrect->Text = Format(L"����� = %d",ARRAYOFCONST((FCountCorrect)));
//	laWrong->Text = Format(L"������� = %d",ARRAYOFCONST((FCountWrong)));
//
//	int xValue1 = Random(20/(FCountWin*(-1)+1));
//	int xValue2 = Random(20/(FCountWin*(-1)+1));
//	int xSign = (Random(2) ==1)?1:-1;
//	int xResult = xValue1+xValue2;
//	int xResultNew = (Random(2) == 1)?xResult : xResult + (Random(10) * xSign);
//
//	FAnswerCorrect = (xResult == xResultNew);
//	laCode->Text = Format("%d + %d = %d",ARRAYOFCONST((xValue1, xValue2, xResultNew)));
//	}

}
//---------------------------------------------------------------------------
void Tfm::DoAnswer(bool aValue)
{
	if(aValue == FAnswerCorrect)
	{
		FCountCorrect++;
		FCountWin++;
	}
	else
	{
		FCountWrong++;
		FCountWin--;
	}
	DoContinue();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRestartClick(TObject *Sender)
{
  DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buYesClick(TObject *Sender)
{
	DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNoClick(TObject *Sender)
{
    DoAnswer(false);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	Randomize();
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage("���� ����������. ��������� ��������� ��������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmTimer(TObject *Sender)
{
	UnicodeString x;
	DateTimeToString(x, L"h:nn:ss.zzz", Now() - FTimerStart);
	laTime->Text = x.Delete(x.Length() - 2, 2);

}
//---------------------------------------------------------------------------

