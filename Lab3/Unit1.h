//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TfmTimer : public TForm
{
__published:	// IDE-managed Components
	TLabel *laTime;
	TButton *buStart;
	TButton *buStop;
	TTimer *tm;
	TButton *Button1;
	TMemo *Memo1;
	TButton *Button2;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
private:
	TDateTime FTimerStart;
public:		// User declarations
	__fastcall TfmTimer(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmTimer *fmTimer;
//---------------------------------------------------------------------------
#endif
