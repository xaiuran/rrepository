//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmTimer *fmTimer;
//---------------------------------------------------------------------------
__fastcall TfmTimer::TfmTimer(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfmTimer::buStartClick(TObject *Sender)
{
	FTimerStart = Now();
	tm->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TfmTimer::buStopClick(TObject *Sender)
{
	tm->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TfmTimer::tmTimer(TObject *Sender)
{
	UnicodeString x;
	DateTimeToString(x, L"h:nn:ss.zzz", Now() - FTimerStart);
    laTime->Text = x.Delete(x.Length() - 2, 2);
	//laTime->Text = TimeToStr(Now() - FTimerStart);
}
//---------------------------------------------------------------------------
void __fastcall TfmTimer::Button1Click(TObject *Sender)
{
	Memo1->Lines->Add(laTime->Text);

}
//---------------------------------------------------------------------------
void __fastcall TfmTimer::Button2Click(TObject *Sender)
{
	Memo1->Lines->Clear();
}
//---------------------------------------------------------------------------
