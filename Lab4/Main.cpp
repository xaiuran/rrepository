//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FCountCat=0;
    FCountStr=0;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::imStrClick(TObject *Sender)
{
	FCountStr++;
	laCountStr->Text = "�������� =" + IntToStr(FCountStr);
	FloatAnimationStr->Start();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::imCatClick(TObject *Sender)
{
	FCountCat++;
	laCountCat->Text = "��� =" + IntToStr(FCountCat);
	FloatAnimationCat->Start();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormResize(TObject *Sender)
{
	FloatAnimation1->StopValue = this->Width - imCat->Width;
	FloatAnimation2->StopValue = this->Width - imStr->Width;
	FloatAnimation3->StopValue = this->Width - imHehCat->Width;
    FloatAnimation4->StopValue = this->Height - imHehCat->Height;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::imHehCatClick(TObject *Sender)
{
	FCountCat--;
	laCountCat->Text = "��� =" + IntToStr(FCountCat);
	FloatAnimationCat->Start();
}
//---------------------------------------------------------------------------
