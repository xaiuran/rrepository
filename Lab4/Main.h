//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TImage *imCat;
	TImage *imStr;
	TLabel *laCountCat;
	TLabel *laCountStr;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TFloatAnimation *FloatAnimationCat;
	TFloatAnimation *FloatAnimationStr;
	TImage *imHehCat;
	TFloatAnimation *FloatAnimation3;
	TFloatAnimation *FloatAnimation4;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall imStrClick(TObject *Sender);
	void __fastcall imCatClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall imHehCatClick(TObject *Sender);
private:	// User declarations
	int FCountCat;
	int FCountStr;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
