//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop
#include "uUtils.h"

#include "formLanGenPassword.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPasswordClick(TObject *Sender)
{
	edPassword->Text = RandomStr(StrToIntDef(edLength->Text,9),
		ckLower->IsChecked, ckUpper->IsChecked, ckNumber->IsChecked, ckSpec->IsChecked, ckNumStr->IsChecked);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"Password Generator");
}
//---------------------------------------------------------------------------
