//---------------------------------------------------------------------------

#ifndef uUtilsH
#define uUtilsH

#include <System.Classes.hpp>

UnicodeString RandomStr(int aLength, bool aLower, bool aUpper, bool aNumber, bool aSpecl, bool aNumStr);
//---------------------------------------------------------------------------
#endif
