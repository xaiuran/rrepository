//---------------------------------------------------------------------------

#ifndef formLanGenPasswordH
#define formLanGenPasswordH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>

//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buAbout;
	TLabel *Label1;
	TLayout *Layout1;
	TEdit *edPassword;
	TCheckBox *ckLower;
	TCheckBox *ckUpper;
	TCheckBox *ckNumber;
	TCheckBox *ckSpec;
	TButton *buPassword;
	TEdit *edLength;
	TLabel *Label2;
	TCheckBox *ckNumStr;
	TStyleBook *StyleBook1;
	void __fastcall buPasswordClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
