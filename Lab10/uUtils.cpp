//---------------------------------------------------------------------------

#pragma hdrstop

#include "uUtils.h"
UnicodeString RandomStr(int aLength, bool aLower, bool aUpper, bool aNumber, bool aSpecl, bool aNumStr){
	const char *c1 = "abcdefghijklmnopqrstuvwxyz";
	const char *c2 = "0123456789";
	const char *c3 = "[]()<>,.;:-+$";
	const char *c4 = "abcdefghijklmnopqrstuvwxyz0123456789";
	UnicodeString x ="";
	UnicodeString xResult = "";
	if (aLower) x += c1;
	if (aUpper) x += UpperCase(c1);
	if (aNumber) x += c2;
	if (aSpecl) x += c3;
	if (aNumStr) x+= c4;
	if (x.IsEmpty()) x =c1;

	while (xResult.Length()< aLength)
		{
			xResult += x.SubString(Random(x.Length() + 1),1);
        }
	
    return xResult;
}
//---------------------------------------------------------------------------
#pragma package(smart_init)
