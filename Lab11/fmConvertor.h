//---------------------------------------------------------------------------

#ifndef fmConvertorH
#define fmConvertorH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TButton *buBack;
	TButton *buAbout;
	TLabel *Label1;
	TTabControl *tc;
	TTabItem *tMenu;
	TTabItem *tTime;
	TTabItem *tDistance;
	TTabItem *tinfo;
	TTabItem *tV;
	TButton *buTime;
	TButton *buDistabce;
	TButton *buInforamtion;
	TButton *buTemperature;
	TGridPanelLayout *GridPanelLayout1;
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TEdit *edMs;
	TEdit *edSec;
	TEdit *edMin;
	TEdit *edHours;
	TEdit *edDays;
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem6;
	TEdit *edMm;
	TListBoxItem *ListBoxItem7;
	TEdit *edSm;
	TListBoxItem *ListBoxItem8;
	TEdit *edM;
	TListBoxItem *ListBoxItem9;
	TEdit *edKm;
	TListBox *ListBox3;
	TListBoxItem *ListBoxItem10;
	TEdit *edBit;
	TListBoxItem *ListBoxItem11;
	TEdit *edBait;
	TListBoxItem *ListBoxItem12;
	TEdit *edKb;
	TListBoxItem *ListBoxItem13;
	TEdit *edMb;
	TListBoxItem *ListBoxItem14;
	TEdit *edGb;
	TListBoxItem *ListBoxItem15;
	TEdit *edTb;
	TListBoxItem *ListBoxItem16;
	TEdit *edPb;
	TListBox *ListBox4;
	TListBoxItem *ListBoxItem17;
	TEdit *edL;
	TListBoxItem *ListBoxItem18;
	TEdit *edCh;
	TListBoxItem *ListBoxItem19;
	TEdit *edVase;
	TListBoxItem *ListBoxItem20;
	TEdit *edVEDRO;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buTimeClick(TObject *Sender);
	void __fastcall buDistabceClick(TObject *Sender);
	void __fastcall buInforamtionClick(TObject *Sender);
	void __fastcall buTemperatureClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall edTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall edAllDistanceKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall edAllInfoKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall edAllVKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
