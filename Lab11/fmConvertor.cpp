//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmConvertor.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->ActiveTab = tMenu;
    tcChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buTimeClick(TObject *Sender)
{
	tc->GotoVisibleTab(tTime->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDistabceClick(TObject *Sender)
{
	 tc->GotoVisibleTab(tDistance->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buInforamtionClick(TObject *Sender)
{
	 tc->GotoVisibleTab(tinfo->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buTemperatureClick(TObject *Sender)
{
	 tc->GotoVisibleTab(tV->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackClick(TObject *Sender)
{
	tc->GotoVisibleTab(tMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"Privet");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tcChange(TObject *Sender)
{
	buBack->Visible = (tc->ActiveTab != tMenu);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::edTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	//
	long double x;
	long double xSec;
	x=StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag) {
	case 1:xSec = x / 1000; break;
	case 2:xSec = x; break;
	case 3:xSec=x* 60  ;break;
	case 4:xSec=x*60 *60  ;break;
	case 5:xSec=x*60*60*60 ;break;
	}
	edMs->Text = FloatToStr(xSec * 1000);
	edSec->Text = FloatToStr(xSec);
	edMin->Text = FloatToStr(xSec /60);
	edHours->Text = FloatToStr(xSec /60/60);
	edDays->Text = FloatToStr(xSec /60/60/24);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::edAllDistanceKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	long double x;
	long double xM;
	x=StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag) {
	case 1:xM = x / 100 / 10; break;
	case 2:xM = x/100; break;
	case 3:xM=x ;break;
	case 4:xM=x*1000  ;break;
	}
	edMm->Text = FloatToStr(xM * 100*10);
	edSm->Text = FloatToStr(xM*100);
	edM->Text = FloatToStr(xM);
	edKm->Text = FloatToStr(xM /1000);

}
//---------------------------------------------------------------------------

void __fastcall Tfm::edAllInfoKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	long double x;
	long double xKb;
	x=StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag) {
	case 1:xKb = x / 8; break;
	case 2:xKb = x; break;
	case 3:xKb=x*1024 ;break;
	case 4:xKb=x*1024*1024  ;break;
	case 5:xKb=x*1024*1024*1024  ;break;
	case 6:xKb=x*1024*1024*1024*1024  ;break;
	case 7:xKb=x*1024*1024*1024*1024*1024  ;break;
	}
	edBit->Text = FloatToStr(xKb * 8);
	edBait->Text = FloatToStr(xKb);
	edKb->Text = FloatToStr(xKb/1024);
	edMb->Text = FloatToStr(xKb/1024/1024);
	edGb->Text = FloatToStr(xKb/1024/1024/1024);
	edTb->Text = FloatToStr(xKb/1024/1024/1024/1024);
	edPb->Text = FloatToStr(xKb/1024/1024/1024/1024/1024);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::edAllVKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	long double x;
	long double xM;
	x=StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag) {
	case 1:xM = x; break;
	case 2:xM = x*10; break;
	case 3:xM=x*50;break;
	case 4:xM=x*10000  ;break;
	}
	edL->Text = FloatToStr(xM);
	edCh->Text = FloatToStr(xM/10);
	edVase->Text = FloatToStr(xM/50);
	edVEDRO->Text = FloatToStr(xM /10000);
}
//---------------------------------------------------------------------------

