//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "vasileva.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Image1Click(TObject *Sender)
{
	FCount++;
	laCount->Text = "Strike: " + IntToStr(FCount);
	FloatAnimationLab -> Start();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
   FCount=0;
}
//---------------------------------------------------------------------------


