//---------------------------------------------------------------------------

#ifndef vasilevaH
#define vasilevaH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TFloatAnimation *FloatAnimation1;
	TLabel *laCount;
	TBitmapListAnimation *BitmapListAnimation1;
	TImage *Image4;
	TBitmapListAnimation *BitmapListAnimation4;
	TFloatAnimation *FloatAnimationLab;
	TImage *Image3;
	TFloatAnimation *FloatAnimation4;
	TBitmapListAnimation *BitmapListAnimation3;
	TImage *Image5;
	TFloatAnimation *FloatAnimation5;
	TBitmapListAnimation *BitmapListAnimation5;
	TImage *Image6;
	TFloatAnimation *FloatAnimation6;
	TBitmapListAnimation *BitmapListAnimation6;
	TImage *Image7;
	TFloatAnimation *FloatAnimation7;
	TBitmapListAnimation *BitmapListAnimation7;
	TImage *Image2;
	TFloatAnimation *FloatAnimation2;
	TBitmapListAnimation *BitmapListAnimation2;
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
	int FCount;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
